﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bazy
{
    public class Blog
    {
        public int BlogID { get; set; }
        [MaxLength(20)]
        public string Name { get; set; }
        public List<Post> Posts { get; set; }
        public string Url { get; set; }

    }
}
