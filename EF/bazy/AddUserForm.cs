﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bazy
{
    public partial class AddUserForm : Form
    {
        public AddUserForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (userNameTextBox.Text == "")
            {
                MessageBox.Show("User name could't be empty.");
                return;
            }
            if (descriptionTextBox.Text == "")
            {
                MessageBox.Show("Description could't be empty.");
                return;
            }
            var user = new User();
            user.UserName = userNameTextBox.Text;
            user.Description = descriptionTextBox.Text;
            using (var bContext = new BlogContext())
            {
                bContext.Users.Add(user);
                bContext.SaveChanges();
            }
            Hide();
            DestroyHandle();
        }
    }
}
