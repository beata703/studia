namespace bazy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBlogIndex : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Blogs", "Name", c => c.String(maxLength: 20));
            CreateIndex("dbo.Blogs", "Name");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Blogs", new[] { "Name" });
            AlterColumn("dbo.Blogs", "Name", c => c.String());
        }
    }
}
