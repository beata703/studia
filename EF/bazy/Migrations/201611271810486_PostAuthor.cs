namespace bazy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostAuthor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "UserName_UserName", c => c.String(maxLength: 128));
            CreateIndex("dbo.Posts", "UserName_UserName");
            AddForeignKey("dbo.Posts", "UserName_UserName", "dbo.Users", "UserName");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Posts", "UserName_UserName", "dbo.Users");
            DropIndex("dbo.Posts", new[] { "UserName_UserName" });
            DropColumn("dbo.Posts", "UserName_UserName");
        }
    }
}
