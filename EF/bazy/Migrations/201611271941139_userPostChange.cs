namespace bazy.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class userPostChange : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Posts", name: "UserName_UserName", newName: "UserName");
            RenameIndex(table: "dbo.Posts", name: "IX_UserName_UserName", newName: "IX_UserName");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Posts", name: "IX_UserName", newName: "IX_UserName_UserName");
            RenameColumn(table: "dbo.Posts", name: "UserName", newName: "UserName_UserName");
        }
    }
}
