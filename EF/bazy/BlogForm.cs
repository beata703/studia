﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace bazy
{
    public partial class BlogForm : Form
    {
        BlogContext bContext;
        public BlogForm()
        {
            InitializeComponent();
        }

        private void BlogForm_Load(object sender, EventArgs e)
        {
            bContext = new BlogContext();
            bContext.Blogs.Load();
            this.blogBindingSource.DataSource = bContext.Blogs.Local.ToBindingList();
        }

        private void blogBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            bContext.SaveChanges();
        }

        private void blogBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            bContext.SaveChanges();
        }

        private void postsBindingSource_CurrentChanged(object sender, EventArgs e)
        {
           
        }

        private void blogDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int blogId = Int32.Parse(this.blogDataGridView.Rows[e.RowIndex].Cells[0].Value.ToString());
;           bContext.Posts.Where(p => p.BlogID == blogId).Load();
            this.postsBindingSource.DataSource = bContext.Posts.Local.ToBindingList();
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            bContext.SaveChanges();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddPostForm addPostForm = new AddPostForm();
            addPostForm.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AddUserForm addUserForm = new AddUserForm();
            addUserForm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SearchForm searchForm = new SearchForm();
            searchForm.ShowDialog();
        }
    }
}
