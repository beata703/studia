﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bazy
{
    public partial class SearchForm : Form
    {
        BlogContext bContext;
        public SearchForm()
        {
            InitializeComponent();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            using (var bContext = new BlogContext())
            {
                string[] blogs = checkedListBox1.CheckedItems.OfType<string>().ToArray();
                var query = bContext.Blogs.Include("Posts").Where(b => blogs.Contains(b.Name)).ToList();
                foreach (var blog in query)
                {
                    treeView1.Nodes.Add(new TreeNode(blog.Name, blog.Posts.Select(y => new TreeNode(y.Title)).ToArray()));
                }              
            }
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            using (var bContext = new BlogContext())
            {
                var blogs = from b in bContext.Blogs
                            orderby b.Name
                            select b.Name;
                foreach (var blog in blogs)
                {
                    checkedListBox1.Items.Add(blog);
                }
            }
        }
    }
}
