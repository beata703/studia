﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bazy
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj nazwe bloga: ");
            string name = Console.ReadLine();         
            Blog blog = new Blog {Name = name};
            BlogContext bContext = new BlogContext();
            bContext.Blogs.Add(blog);
            bContext.SaveChanges();

           
            Console.WriteLine("Nazwy blogów malejąco - query syntax: ");
            var blogNamesDesc = from b in bContext.Blogs
                        orderby b.Name descending
                        select b.Name;
            foreach( var item in blogNamesDesc) {
                Console.WriteLine(item);
            }


            //lazy loading
            Console.WriteLine("Nazwy blogów - method syntax: ");
            foreach (var item in bContext.Blogs.Select(b => b.Name).ToList())
            {
                Console.WriteLine(item);
            }

            
            Console.WriteLine("Blogi i posty - joiny - query syntax: ");
            var blogsPostsJoin = from b in bContext.Blogs
                join p in bContext.Posts on b.BlogID equals p.BlogID into posts             
                select new
                {
                    Blog = b.Name,
                    PostsQuantity = posts.Count(),
                    Posts = posts
                };
            foreach (var item in blogsPostsJoin)
            {
                Console.WriteLine("Blog: " + item.Blog);
                Console.WriteLine("Posts number: " + item.PostsQuantity);
                Console.WriteLine("Posts: ");
                foreach (var post in item.Posts)
                {
                    Console.WriteLine(post.Title);
                }
                Console.WriteLine();
            }

            
            Console.WriteLine("Blogi i posty - joiny - method syntax: ");
            var blogsPostsJoin2 = bContext.Blogs.GroupJoin(bContext.Posts, b => b.BlogID, p => p.BlogID,
                (b, bGroup) => new
                {
                    Blog = b.Name,
                    PostsQuantity = bGroup.Count(),
                    Posts = bGroup
                });
            foreach (var item in blogsPostsJoin2)
            {
                Console.WriteLine("Blog: " + item.Blog);
                Console.WriteLine("Posts number: " + item.PostsQuantity);
                Console.WriteLine("Posts: ");
                foreach (var post in item.Posts)
                {
                    Console.WriteLine(post.Title);
                }
                Console.WriteLine();
            }

            
            Console.WriteLine("Blogi i posty - navigation properties - query syntax: ");
            var blogsPostsNavigationProperties = (from b in bContext.Blogs
                                                 select b).Include(b => b.Posts);
            foreach (Blog item in blogsPostsNavigationProperties)
            {
                Console.WriteLine("Blog: " + item.BlogID);
                Console.WriteLine("Posts number: " + item.Posts.Count);
                Console.WriteLine("Posts: ");
                foreach (Post post in item.Posts)
                {
                    Console.WriteLine(post.Title);
                }
                Console.WriteLine();
            }

            
            Console.WriteLine("Blogi i posty - navigation properties - method syntax: ");
            var blogsPostsNavigationProperties2 = bContext.Blogs;
            foreach (var item in blogsPostsNavigationProperties2)
            {
                Console.WriteLine("Blog: " + item.BlogID);
                Console.WriteLine("Posts number: " + item.Posts.Count);
                Console.WriteLine("Posts: ");
                foreach (var post in item.Posts)
                {
                    Console.WriteLine(post.Title);
                }
                Console.WriteLine();
            }

            //lazy loading juz bylo wyzej
            Console.WriteLine("Blogi i posty - eager loading: ");
            var blogsPostsEagerLoading = from b in bContext.Blogs
                                                 select b;
            foreach (var item in blogsPostsEagerLoading)
            {
                Console.WriteLine("Blog: " + item.BlogID);
                Console.WriteLine("Posts number: " + item.Posts.Count);
                Console.WriteLine("Posts: ");
                foreach (var post in item.Posts)
                {
                    Console.WriteLine(post.Title);
                }
                Console.WriteLine();
            }


            BlogForm bForm = new BlogForm();
            bForm.ShowDialog();

            Console.ReadKey();
        }
    }
}
