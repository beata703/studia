﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bazy
{
    public partial class AddPostForm : Form
    {
        public AddPostForm()
        {
            InitializeComponent();
            this.Load += AddPostForm_Load;
        }
        private void AddPostForm_Load(object sender, EventArgs e)
        {
            blogComboBox.Items.Add("Select blog...");
            blogComboBox.SelectedItem = blogComboBox.Items[0];
            userComboBox.Items.Add("Select user...");
            userComboBox.SelectedItem = userComboBox.Items[0];
            using (var bContext = new BlogContext())
            {
                var blogs = from b in bContext.Blogs
                            orderby b.Name
                            select b.Name;
                foreach (var blog in blogs)
                {
                    blogComboBox.Items.Add(blog);
                }

                var users = bContext.Users.OrderBy(u => u.UserName).Select(u => u.UserName).ToList();
                foreach (var user in users)
                {
                    userComboBox.Items.Add(user);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var selectedBlog = (string)blogComboBox.SelectedItem;
            var selectedUser = (string) userComboBox.SelectedItem;
            int blogId;

            using (var bContext = new BlogContext())
            {
                blogId = (from b in bContext.Blogs where b.Name == selectedBlog select b.BlogID).FirstOrDefault();
                if (blogId == 0)
                {
                    MessageBox.Show("Blog should be selected.");
                    return;
                }              
            }

            if (selectedUser == "")
            {
                MessageBox.Show("User should be selected.");
                return;
            }
            if (titleTextBox.Text == "")
            {
                MessageBox.Show("Title could't be empty.");
                return;
            }
            if (contentTextBox.Text == "")
            {
                MessageBox.Show("Post could't be empty.");
                return;
            }

            var post = new Post();
            post.BlogID = blogId;
            post.Content = contentTextBox.Text;
            post.Title = titleTextBox.Text;           
            post.UserName = selectedUser;
            
            using (var bContext = new BlogContext())
            {
                bContext.Posts.Add(post);
                bContext.SaveChanges();
            }
            Hide();
            DestroyHandle();
        }

    }
}
