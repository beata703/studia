﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bazy
{
    public class BlogContext: DbContext
    {
        public BlogContext() : base("EFDbConnectionString")
        {
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
            .Property(u =>
            u.Description)
            .HasColumnName("DetailedDescription");

            modelBuilder.Entity<Blog>()
                .Property(b => b.Name)
                .HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute()));
        } 
    }
}
